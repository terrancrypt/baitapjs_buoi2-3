// Bài 1: Tính tiền lương nhân viên
document.getElementById("btnTienLuong").onclick = function () {
  /*
    Input: 
          + tienluong1Ngay: number
          + soNgayLamViec;:number
    Progress:
          + Lấy thông tin của người dùng từ giao diện
          + Sử dụng công thức tính tiền lương sau đó ỉn kết quả ra giao diện
    Output:
         + tongTienLuong: number

    */
  var tienLuong1Ngay = document.getElementById("tienLuong1Ngay").value;
  var soNgayLamViec = document.getElementById("soNgayLamViec").value;
  console.log(tienLuong1Ngay);
  console.log(soNgayLamViec);
  var tongTienLuong = 0;

  tongTienLuong = tienLuong1Ngay * soNgayLamViec;

  var tagTienLuong = document.getElementById("tongTienLuong");
  tagTienLuong.innerHTML =
    "Số tiền lương của bạn là: " + tongTienLuong.toLocaleString() + "vnđ";
  tagTienLuong.className = "alert alert-success ml-2 mb-0";
};

// Bài 2: tính giá trị trung bình
document.getElementById("btnTrungBinh").onclick = function () {
  /*
    Input: 
         + soThu1, soThu2, soThu3, soThu4, soThu5: number
    Progress:
         + Lấy giá trị số người dùng nhập vào từ giao diện
         + Ép kiểu dữ liệu thành number
         + Sử dụng công thức tính giá trị trung bình sau đó in kết quả ra giao diện
    Output: 
         + soTrungBinh: number
    */
  var soThu1 = document.getElementById("soThu1").value;
  var soThu2 = document.getElementById("soThu2").value;
  var soThu3 = document.getElementById("soThu3").value;
  var soThu4 = document.getElementById("soThu4").value;
  var soThu5 = document.getElementById("soThu5").value;
  var soTrungBinh = 0;

  var a = Number(soThu1);
  var b = Number(soThu2);
  var c = Number(soThu3);
  var d = Number(soThu4);
  var e = Number(soThu5);

  soTrungBinh = (a + b + c + d + e) / 5;

  var tagTrungBinh = document.getElementById("soTrungBinh");
  tagTrungBinh.innerHTML = "Số trung bình của bạn là: " + soTrungBinh;
  tagTrungBinh.className = "alert alert-success ml-2 mb-0";
};

// Bài 3: Quy đổi tiền tệ
document.getElementById("btnQuyDoi").onclick = function () {
  /*
    Input:
         + soTienUSD: number;
    Progress
         + Lấy giá trị người dùng nhập vào từ giao diện
         + Sử dụng công thức tính giá trị tiền quy đổi từ USD sang VND và in kết quả ra màn hình
    Ouput:
         + soTienVND: number
    */
  var soTienUSD = document.getElementById("tienUSD").value;
  var tyGia = 23500;
  var soTienVND = 0;

  soTienVND = soTienUSD * tyGia;

  tagTienTe = document.getElementById("quyDoiTien");
  tagTienTe.innerHTML = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  }).format(soTienVND);
  tagTienTe.className = "alert alert-success ml-2 mb-0";
};

// Bài 4: Tính diện tích và chu vi của hình chữ nhật
document.getElementById("btnTinhDTCV").onclick = function () {
  /*
    Input:
         + chieuDai: number;
         + chieuRong: number;
    Progress:
         + Lấy giá trị người dùng nhập vào từ giao diện
         + Sử dụng công thức tính diện tích và chu vi hình chữ nhật để tính ra kết quả và in ra giao diện
    Output:
         + dienTich: number;
         + chuVi: number;
    */
  var chieuDai = document.getElementById("chieuDai").value;
  var chieuRong = document.getElementById("chieuRong").value;
  var chieuDai2 = Number(chieuDai);
  var chieuRong2 = Number(chieuRong);
  var dienTich = 0;
  var chuVi = 0;

  dienTich = chieuDai * chieuRong;
  chuVi = (chieuDai2 + chieuRong2) * 2;

  var tagDTCV = document.getElementById("tinhDTCV");
  tagDTCV.innerHTML = "Diện tích = " + dienTich + "; Chu vi = " + chuVi;
  tagDTCV.className = "alert alert-success ml-2 mb-0";
};

// Bài 5: Tính tổng 2 ký số
document.getElementById("btnKySo").onclick = function () {
  /*
    Input:
         + so2ChuSo: number
    Progress:
         + Lấy giá trị người dùng nhập vào từ giao diện
         + Sử dụng công thức tính tổng 2 ký số và in ra giao diện
    Output:
         + tong2KySo: number
    */
  var so2ChuSo = document.getElementById("so2ChuSo").value;
  var tong2KySo = 0;

  var hangDonVi = Math.floor(so2ChuSo % 10);
  var hangChuc = Math.floor(so2ChuSo / 10);

  tong2KySo = hangDonVi + hangChuc;

  var tagKySo = document.getElementById("tinhKySo");
  tagKySo.innerHTML = "Tổng 2 ký số = " + tong2KySo;
  tagKySo.className = "alert alert-success ml-2 mb-0";
};
